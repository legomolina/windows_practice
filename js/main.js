var MAX_WINDOWS = 3;
var TIME_BETWEEN_OPEN = 3000;

var MIN_TOP_POSITION = 50;
var MAX_TOP_POSITION = 400;
var MIN_LEFT_POSITION = 50;
var MAX_LEFT_POSITION = 900;

var windows = [];
var closeWindowsOrder = [];
var closeWindowsOrderIndex = 0;

var windowSelectors = [];

var currentTimeout = null;
var isRunning = false;

document.getElementById("startButton").onclick = start;
document.getElementById("stopButton").onclick = stop;

function start() {
    if(!checkItemsSelected())
        alert("No se pueden repetir los valores.");

    else if(!isRunning) {
        isRunning = true;

        currentTimeout = setTimeout(function() {
            openWindow(0);
        }, TIME_BETWEEN_OPEN);
    }
}

function stop() {
    if(isRunning) {
        isRunning = false;

        clearTimeout(currentTimeout);
    }
}

function checkItemsSelected() {
    closeWindowsOrder = [];
    var selects = document.querySelectorAll("select");

    for(var i = 0; i < selects.length; i++) {
        var value = parseInt(selects[i].options[selects[i].options.selectedIndex].value);

        if(closeWindowsOrder.indexOf(value) >= 0)
            return false;

        closeWindowsOrder.push(value);
    }

    return true;
}

function openWindow(windowIndex) {
    var win;
    var windowDocument;
    var position = {
        top: calculateRandomNumber(MIN_TOP_POSITION, MAX_TOP_POSITION),
        left: calculateRandomNumber(MIN_LEFT_POSITION, MAX_LEFT_POSITION)
    };

    win = window.open(null, null, "top=" + position.top + ", left=" + position.left + ", width=300, height=300, toolbar=yes");

    windowDocument = win.document;

    windowDocument.open();

    var container = windowDocument.createElement("div");
    var paragraph = windowDocument.createElement("p");

    paragraph.innerHTML = "Esta es la página " + windowIndex  + ".";
    container.appendChild(paragraph);

    var paragraph = windowDocument.createElement("p");
    paragraph.innerHTML = "Se está ejecutando en el navegador " + checkBrowser() + ".";
    container.appendChild(paragraph);
    
    windowDocument.appendChild(container);

    windows[windowIndex] = win;
    
    if (++windowIndex < MAX_WINDOWS)
        currentTimeout = setTimeout(function() {
            openWindow(windowIndex);
        }, TIME_BETWEEN_OPEN);

    else
        currentTimeout = setTimeout(function() {
            closeWindow(closeWindowsOrder[closeWindowsOrderIndex]);
        }, 5000);
}

function closeWindow(windowIndex) {
    if (getOpenWindowIndexes().indexOf(windowIndex) >= 0)
        windows[windowIndex].close();

    if(++closeWindowsOrderIndex < closeWindowsOrder.length)
        currentTimeout = setTimeout(function() {
            closeWindow(closeWindowsOrder[closeWindowsOrderIndex]);
        }, TIME_BETWEEN_OPEN);
}

function getOpenWindowIndexes() {
    var openedWindows = [];

    for(var i = 0; i < windows.length; i++) {
        if(!windows[i].closed)
            openedWindows.push(i);
    }

    return openedWindows;
}

function calculateRandomNumber(min, max) {
    return Math.floor(Math.random() * max) + min;
}